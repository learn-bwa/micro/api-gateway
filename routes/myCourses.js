var express = require('express');
var router = express.Router();
const { APP_NAME } = process.env;
const myCourses = require('./handler/my-coureses');

router.post('/', myCourses.create)
router.get('/', myCourses.get)

module.exports = router;
