const express = require('express');
const router = express.Router();
const userHandler = require('./handler/user');
const verifyToken = require('../middleware/verifyToken');

const { APP_NAME } = process.env;

router.post('/register', userHandler.register);
router.post('/login', userHandler.login);
router.put('/update', verifyToken, userHandler.update);
router.get('/', verifyToken, userHandler.getUser);
router.post('/logout', verifyToken, userHandler.logout);

module.exports = router;
