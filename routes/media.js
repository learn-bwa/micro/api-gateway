const Router = require('express');
const mediaHandler = require('./handler/media');

const router = Router();
const { APP_NAME } = process.env;

router.post('/', mediaHandler.create);
router.get('/', mediaHandler.getAll);
router.delete('/:id', mediaHandler.destroy);

module.exports = router;
