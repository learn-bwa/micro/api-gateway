const Router = require('express');
const mentorHandler = require('./handler/mentors');

const router = Router();
const { APP_NAME } = process.env;

router.get('/', mentorHandler.getAll)
router.get('/:id', mentorHandler.get)
router.post('/', mentorHandler.create)
router.put('/:id', mentorHandler.update)
router.delete('/:id', mentorHandler.destroy)
module.exports = router;
