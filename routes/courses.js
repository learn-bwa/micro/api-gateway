var express = require('express');
var router = express.Router();
const { APP_NAME } = process.env;
const courseHandler = require('./handler/courses');

const verifyToken = require('../middleware/verifyToken')

router.get('/', courseHandler.getAll)
router.get('/:id', courseHandler.get)

router.post('/',verifyToken, courseHandler.create)
router.put('/:id',verifyToken, courseHandler.update)
router.delete('/:id',verifyToken, courseHandler.destroy)

module.exports = router;
