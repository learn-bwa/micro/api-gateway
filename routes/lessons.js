var express = require('express');
var router = express.Router();
const { APP_NAME } = process.env;
const lessonHandler = require('./handler/lessons');

router.get('/', lessonHandler.getAll)
router.get('/:id', lessonHandler.get)
router.post('/',lessonHandler.create)
router.put('/:id',lessonHandler.update)
router.delete('/:id',lessonHandler.destroy)

module.exports = router;
