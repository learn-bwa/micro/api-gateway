const create = require('./create');
const destroy = require('./destroy');
const get = require('./get');
const getAll = require('./getAll');
const update = require("./update");

module.exports = {
    create,update, destroy, get, getAll
}