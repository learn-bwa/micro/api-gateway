const apiAdapter = require('../../apiAdapter');

const { URL_SERVICE_COURSE } = process.env;

const api = apiAdapter(URL_SERVICE_COURSE);

module.exports = async (req, res, next) => {
    try {
        const userId = req.user.data.id;
        const id = req.params.id;
        
        const reviews = await api.put(`/api/reviews/${id}`, {
            user_id: userId,
            ...req.body
        });
        return res.json(reviews.data);
    } catch (error) {
        if (error === 'ECONNREFUSED') {
            return res.status(500).json({
                status: 'error',
                message: 'service unavailable'
            })
        }

        const { status, data } = error.response;

        return res.status(status).json(data);
    }
}