var express = require('express');
var router = express.Router();
const { APP_NAME } = process.env;
const imageCourses = require('./handler/image-courses');

const verifyToken = require('../middleware/verifyToken')

router.post('/',verifyToken, imageCourses.create)
router.delete('/:id',verifyToken, imageCourses.destroy)

module.exports = router;
