const express = require('express');
const router = express.Router();
const refreshTokenHandler = require('./handler/refresh-token');

const { APP_NAME } = process.env;

router.post('/', refreshTokenHandler.refreshToken);

module.exports = router;
